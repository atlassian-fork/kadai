package kadai

import org.scalacheck.Prop

import scalaz._
import scalaz.std.anyVal._
import scalaz.std.option._
import scalaz.syntax.all._
import scalaz.syntax.std.option._
import ScalazArbitrary._
import ScalazProperties._

class AttemptSpec extends ScalaCheckSpec with EqualSyntax {
  import Invalid.syntax._
  import Arbitraries._

  def is = s2"""
  Attempt should
    be creatable from a String               $fromString
    be creatable from a Throwable            $fromThrowable
    be creatable from anything               $fromAny
    be safely creatable from exception       $safely
    be safely mappable                       $safelyMap
    be safely flatMappable                   $safelyFlatMap
    run lifted Result functions              $liftResultFn
    convert to option                        $toOption
    convert to disjunction                   $toOr
    have a law abiding Monad                 ${checkAll(monad.laws[Attempt])}
    have a law abiding Equal                 ${checkAll(equal.laws[Attempt[Int]])}
    have a law abiding Monoid                ${checkAll(monoid.laws[Attempt[Int]])}
    provide invalid values via a Catchable   $testCatchableAttempt
    provide failures via a Catchable         $testCatchableFail
    delegate Catchable work                  $testCatchableImplicit
  """

  def fromString =
    Prop.forAll { s: String =>
      Attempt.fail(s) must beEqualTo(Attempt(s.invalidResult))
    }

  def fromThrowable =
    Prop.forAll { t: Throwable =>
      Attempt.exception(t) must beEqualTo(Attempt(t.invalidResult))
    }

  def fromAny =
    Prop.forAll { a: Int =>
      Attempt.ok(a) must beEqualTo(Attempt(a.right))
    }

  def safely =
    checkSafe { identity }

  def safelyMap =
    checkSafe { _.map { _ + 11 } }

  def safelyFlatMap =
    checkSafe { _.flatMap { _ => Attempt.exception(new Error) } }

  private def checkSafe(f: Attempt[Int] => Attempt[Int]) =
    new RuntimeException |> { ex =>
      f { Attempt.safe { throw ex } } === Attempt(ex.invalidResult)
    }

  def liftResultFn =
    Prop.forAll { (fa: Attempt[Int], f: Invalid \/ Int => Invalid \/ Int) =>
      Attempt(f(fa.run)) mustEqual fa.lift(f)
    }

  def toOr =
    Prop.forAll { i: Int =>
      (Attempt.ok(i).toOr mustEqual i.right) and (Attempt.ToOr(Attempt.ok(i)) mustEqual i.right)
    }

  def toOption =
    Prop.forAll { i: Int =>
      (Attempt.ok(i).toOption mustEqual i.some) and (Attempt.ToOption(Attempt.ok(i)) mustEqual i.some)
    }

  def testCatchableAttempt =
    Prop.forAll { ata: Attempt[Int] =>
      val run = Attempt.AttemptCatchable.attempt(ata).run.toEither
      ata.run.leftMap(Invalid.WrappedException.apply).toEither
      (run must beRight) and (run.right.get.toEither mustEqual ata.run.leftMap(Invalid.WrappedException.apply).toEither)
    }

  def testCatchableFail =
    Prop.forAll { t: Throwable =>
      Attempt.AttemptCatchable.fail(t) must beEqualTo(Attempt(Invalid.Err(t).left))
    }

  def testCatchableImplicit =
    Prop.forAll { ata: Attempt[Int] =>
      type Action[A] = ReaderT[Attempt, Int, A]
      def action[A](f: Int => Attempt[A]): Action[A] = Kleisli { f }
      val c: Catchable[Action] = scalaz.Kleisli.kleisliCatchable[kadai.Attempt, Int]
      val run: Either[Invalid, Throwable \/ Int] = c.attempt(action(_ => ata)).run(1).run.toEither
      (run must beRight) and (run.right.get.toEither mustEqual ata.run.leftMap(Invalid.WrappedException.apply).toEither)
    }
}