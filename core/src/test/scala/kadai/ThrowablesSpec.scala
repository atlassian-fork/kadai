package kadai

import org.junit.runner.RunWith
import org.scalacheck.Prop
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ThrowablesSpec extends ScalaCheckSpec {

  def is = s2"""
  Throwables should
    convert a Throwable to a String               $throwableAsString
  """

  def throwableAsString =
    Prop.forAll { t: Throwable =>
      { Throwables.asString(t) must startWith(t.getClass.getName) } and
        { Throwables.asString(t) must contain("at ") }

    }
}
