package kadai.concurrent

import scalaz.{ \/, \/-, -\/ }

/**
 * Simple memoizer that can fail.
 *
 * WARNING it must be used with care. Note in particular that it must not
 * always fail as it tail recurses in that case and will live lock.
 */
final class Memoize[E, A](f: => E \/ A) {
  import Memoize._

  private val atom: Atomic[Ref[E, A]] = Atomic(Empty)

  @annotation.tailrec
  def get: A =
    atom.get match {
      case Value(a) => a
      case exec @ Executing() => exec.await match {
        case -\/(_) => get
        case \/-(a) =>
          if (atom.get == exec && atom.compareAndSet(exec, Value(a)))
            a
          else
            get
      }
      case Empty =>
        atom.compareAndSet(Empty, Executing()(f))
        get
    }

}

object Memoize {
  private[Memoize] sealed trait Ref[+E, +A]
  private[Memoize] case class Executing[E, A]()(f: => E \/ A) extends Ref[E, A] {
    lazy val await = f
  }
  private[Memoize] case class Value[E, A](a: A) extends Ref[E, A]
  private[Memoize] case object Empty extends Ref[Nothing, Nothing]
}