package kadai.config

import reflect.ClassTag
import com.typesafe.config.Config

/**
 * Used to extract scala object instances that implement some provider style interface.
 *
 * Use:
 *
 *    config[Module[Foo]]("name").get
 *
 * where Foo is the trait that your object implements, and 'name' is config property
 * that holds the name of the actual object
 */
final case class Module[A](get: A)

object Module {
  implicit def ModuleAccessor[A] = new Configuration.Accessor[Module[A]] {
    def apply(c: Config, s: String): Module[A] = {
      val m = scala.reflect.runtime.universe.runtimeMirror(getClass.getClassLoader)
      Module(
        m.reflectModule(m.staticModule(c.getString(s))).instance.asInstanceOf[A]
      )
    }
  }
}